import sys
from utils.qiankun_log_parser import QiankunLogParser



if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Pls select the parse file path")
        print('Usage: python qiankun_log_parser_test.py "\\qadata\shqa\Qiankun\logs for comparison\23Q1_1.9.1.0001_0210_log"')
        exit(-1)
    target_file_path = sys.argv[1]
    log_p_target = QiankunLogParser(target_file_path, "qiankun", "result_target.txt", "result_encap_target.txt")
    log_p_target.parse()
    first, second, third, fourth = log_p_target.parse()
    print("First: ", first)
    print("Second: ", second)
    print("Third: ", third)
    print("Forth: ", fourth)