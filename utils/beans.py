from collections import namedtuple
from recordclass import recordclass

GlobalTimeFormat = '%Y-%m-%d %H:%M:%S.%f'
EnCapMaxMilliSecsDiff = 3999
LocDataStr = namedtuple("LocDataStr", ["loc_s", "data_s"])
LocData = namedtuple("LocData", ["loc", "data"])
SendData = namedtuple("SendData", ["timestamp", "data"])
Loc = namedtuple("Loc", ["timestamp", "loc"])

ExceedInfo = namedtuple("ExceedInfo", ["exceed", "value"])
SignValueMap = namedtuple("SignValueMap" , ["sign_name", "value_map", "exceed"])

Detail = namedtuple("Detail", ["sign_name", "key", "value"])

EncapData = recordclass("EncapData", ["timestamp", "position_msg", "sign_name", "key", "value", "associated_sign", "fail_reason"])

LocAndSignDetails = namedtuple("LocAndSignDetails", ["loc", "details"])

ParseResult = namedtuple("ParseResult", ["data_ls", "loc_data_ls", "loc_data_ls_raw",
                                         "encap_data_ls"])


class FailReasons(object):
    FailNumNotEq = "FailNumNotEq"
    FailDataNotMatch = "FailDataNotMatch"
    # Encap data related fail reasons
    # ISATrafficSignType
    FailEncapTargetNotCorrect = "FailEncapTargetNotCorrect"
    FailEncapDataNotMatch = "FailEncapDataNotMatch"
    FailEncapNumNotEq = "FailEncapNumNotEq"