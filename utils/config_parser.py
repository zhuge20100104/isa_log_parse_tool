from utils.beans import SignValueMap
import io
import json
from .beans import ExceedInfo


class ConfigParser(object):
    def parse(self):
        with io.open("./sign_value_map.json", "r", encoding="utf-8") as sign_value_conf_f:
            sign_value_map_raw_ls = json.load(sign_value_conf_f)
        sign_value_m_list = list()

        for sign_value_m_raw_obj in sign_value_map_raw_ls:
            exceed = None
            if "exceed" in sign_value_m_raw_obj:
                exceed = ExceedInfo(sign_value_m_raw_obj["exceed"]["exceed"], \
                                     sign_value_m_raw_obj["exceed"]["value"])
            value_map = sign_value_m_raw_obj["value_map"]
            sign_name = sign_value_m_raw_obj["sign_name"]
            sign_value_m = SignValueMap(sign_name, value_map, exceed)
            sign_value_m_list.append(sign_value_m)

        return sign_value_m_list
