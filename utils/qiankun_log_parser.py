from subprocess import Popen, PIPE 
import os 
import re
from .funcs import  parse_key_val_pair
import io 
from .config_parser import ConfigParser
from .beans import *
import json

class QiankunLogParser(object):
    def __init__(self, log_folder, parse_way, result_path, encap_result_path = None):
        self.log_folder = log_folder
        self.parse_way = parse_way
        self.result_path = result_path
        self.encap_result_path = encap_result_path
        self.log_mapping = {
            "qiankun": 1
        }

    def wait_expect_str(self, p, expected_s):
        while True:
            line = p.stdout.readline().decode("utf-8") 
            print(line)
            if line.find(expected_s) != -1:
                break
    
    def std_in_write_and_flush(self, p, write_s):
        p.stdin.write(write_s)
        p.stdin.flush()

    def convert_using_isa_toolkit(self):
        p = Popen(["java", "-jar", "ISAToolkit.jar"], stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
        self.wait_expect_str(p, "Exit")
        self.std_in_write_and_flush(p, b"2\r\n")
        
        self.wait_expect_str(p, "Qiankun")
        parse_way_ = self.log_mapping[self.parse_way]
        parse_s = bytes("{}\r\n".format(parse_way_), 'utf-8')
        self.std_in_write_and_flush(p, parse_s)

        self.wait_expect_str(p, "CanLog")
        log_folder_s = bytes("{}\r\n".format(self.log_folder), 'utf-8')
        self.std_in_write_and_flush(p, log_folder_s)
        
        self.wait_expect_str(p, "Exit")
        self.std_in_write_and_flush(p, b"1\r\n")
        
        self.wait_expect_str(p, "Exit")
        self.std_in_write_and_flush(p, b"9\r\n")
        
        res, err =p.communicate()
        print(res)
        print(err)
        assert err == b"", "Convert to result.txt using ISA toolkit failed..."
    
    def parse_one_line(self, line):
        # MMGPS(latLon=LatLon(lat=41.3889967, lon=2.156348), adasPosition=AdasPosition(positionMessage=2309760826875068646,
        #  canMsg=CanMsg(data=[10, 2, 2, 0, 8191, 1, 0, 0, 2, 197, 1, 1], elapsedTime=15739200))))
        lat_lon_g = re.match(".*data=(\[.*?\]).*positionMessage=(.*?)\,.*mmAccurateGps=LatLon\(lat=(.*?)\, lon=(.*?)\).*", line)
        if lat_lon_g is None:
            return None
        data_str = lat_lon_g.group(1)
        pos_msg = lat_lon_g.group(2)
        lat = lat_lon_g.group(3)
        lon = lat_lon_g.group(4)
        
        loc_str = "[{}, {}]".format(lat, lon)
        loc = Loc(pos_msg, json.loads(loc_str))
        data = SendData(pos_msg, json.loads(data_str))
        return LocData(loc, data)

    def parse_to_raw_data(self):
        loc_and_data_ls = list()
        with io.open("result.txt", mode='r', encoding="utf-8") as result_f:
            for line in result_f.readlines():
                # Check if sendData canMsg exist in the current record
                if line.find("CanMsg") != -1:
                    loc_data = self.parse_one_line(line)
                    if loc_data is None:
                        continue
                    loc_and_data_ls.append(loc_data)
        return loc_and_data_ls
    
    def parse_one_line_of_encap_data(self, line):
        # ENCapMsg(line=68, timestamps=2023-03-15 17:20:31.821, positionMessage=2352999121620586234, trafficSign=2, elapsedTime=437929297)
        # ENCapMsg(line=77, timestamps=2023-03-15 17:20:34.827, positionMessage=null, trafficSign=0(2 timeout), elapsedTime=437932303)
        DEFAULT_ASSOCIATED_SIGN = 0
        associated_sign = DEFAULT_ASSOCIATED_SIGN
        if line.find("positionMessage=null") == -1:
            traffic_sign_g = re.match(".*timestamps=(.*?)\,.*positionMessage=(.*?)\,.*trafficSign=(.*?)\,.*", line)
            if traffic_sign_g is None:
                return None
            time_stamp_str = traffic_sign_g.group(1)
            position_msg_str = traffic_sign_g.group(2)
            traffic_sign_str = int(traffic_sign_g.group(3))
        else:
            traffic_sign_g = re.match(".*timestamps=(.*?)\,.*positionMessage=(.*?)\,.*trafficSign=(.*?)\,.*\((.*?) timeout\).*", line)
            if traffic_sign_g is None:
                return None
            time_stamp_str = traffic_sign_g.group(1)
            position_msg_str = None  if traffic_sign_g.group(2) == 'null' else traffic_sign_g.group(2) 
            traffic_sign_str = int(traffic_sign_g.group(3))
            associated_sign = int(traffic_sign_g.group(4))

        return {"timestamp": time_stamp_str, "position_msg": position_msg_str,  \
                "traffic_sign": traffic_sign_str, "associate_sign": associated_sign }
    
    def parse_to_final_data(self, loc_and_data_ls):
        conf_parser = ConfigParser()
        sign_value_map_list = conf_parser.parse()

        loc_and_details_list = list()
        for loc_and_data in loc_and_data_ls:
            loc_ = loc_and_data.loc 
            data = loc_and_data.data
            data_ = data.data
            details_ = list()
            for idx, val in enumerate(data_):
                sign_value_map = sign_value_map_list[idx]
                detail = parse_key_val_pair(val, sign_value_map)
                details_.append(detail)
            loc_and_details_list.append(LocAndSignDetails(loc_, details_))
        
       
        if os.path.exists(self.result_path):
            os.remove(self.result_path)
        os.rename("result.txt", self.result_path)
        return None, loc_and_details_list, loc_and_data_ls

    def get_current_sign_value_map(self, sign_name):
        conf_parser = ConfigParser()
        sign_value_map_list = conf_parser.parse()
        current_sign_map = None
        for sign_value_map in sign_value_map_list:
            if sign_value_map.sign_name == sign_name:
                current_sign_map = sign_value_map
                break
        return current_sign_map

    # Encap data for ISATrafficSignType
    def parse_encap_data(self):
        result = list()
        if self.encap_result_path is None:
            print("No Encap result should be returned")
            return result
        
        encap_data_raw_ls = list()
        with io.open("encap_result.txt", mode='r', encoding="utf-8") as result_f:
            for line in result_f.readlines():
                # Check if sendData canMsg exist in the current record
                if line.find("ENCapMsg") != -1:
                    encap_data = self.parse_one_line_of_encap_data(line)
                    if encap_data is None:
                        continue
                    encap_data_raw_ls.append(encap_data)

        # print("Encap raw data list: ", encap_data_raw_ls)
        current_sign_map = self.get_current_sign_value_map("前方交通标志类型")
        # print("Current sign value map: ", current_sign_map)

        ENCAP_DATA_DEF_FAIL_REASON = "success"
        for encap_data_raw in encap_data_raw_ls:
            timestamp = encap_data_raw["timestamp"]
            position_msg = encap_data_raw["position_msg"]
            key = encap_data_raw["traffic_sign"]
            key_s = str(key)
            sign_name = current_sign_map.sign_name
            sign_value = None
            if key_s in current_sign_map.value_map:
                sign_value = current_sign_map.value_map[key_s]
            associated_sign = encap_data_raw["associate_sign"]
            e_data = EncapData(timestamp, position_msg, sign_name, key, sign_value, associated_sign,\
                                ENCAP_DATA_DEF_FAIL_REASON)
            result.append(e_data)
        
        # Save the current encap result file to keep isolating
        if os.path.exists(self.encap_result_path):
            os.remove(self.encap_result_path)
        os.rename("encap_result.txt", self.encap_result_path)
        return result


    def parse(self)-> ParseResult:
        # TODO: revert it back
        self.convert_using_isa_toolkit()
        loc_and_data_ls = self.parse_to_raw_data()
        en_cap_data = self.parse_encap_data()
        data_ls, loc_data_ls, loc_data_ls_raw = self.parse_to_final_data(loc_and_data_ls)
        return ParseResult(data_ls, loc_data_ls, loc_data_ls_raw, en_cap_data)

       
