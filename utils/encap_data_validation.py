
from .beans import EncapData, GlobalTimeFormat, EnCapMaxMilliSecsDiff
from queue import Queue
from datetime import datetime
import pandas as pd


class EncapDataValidation(object):
    def __init__(self, encap_data_ls_target, encap_data_ls_expect):
        self.encap_data_ls_target = encap_data_ls_target
        self.encap_data_ls_expect = encap_data_ls_expect
        self.excel_writer = pd.ExcelWriter("encap_compare_result.xlsx")
    
    def valid_single_data(self, encap_data: EncapData):
        if encap_data.key != 0:
            if encap_data.associated_sign != 0:
                encap_data.fail_reason = "key not zero, associate_key should be zero"
                return False
        
        if encap_data.key == 0:
            if encap_data.associated_sign == 0:
                encap_data.fail_reason = "Key is zero, associate_key should not be zero"
                return False
        return True
            
    def valid_single_file(self):
        q = Queue()
        passed_pairs = 0
        failed_pairs = 0
        wrong_data_list = list()
        for encap_data in self.encap_data_ls_target:
            if not self.valid_single_data(encap_data):
                wrong_data_list.append(encap_data)
                failed_pairs += 1
                continue
            if encap_data.key != 0:
                q.put(encap_data)
            else:
                is_matched_key_found = False
                # encap_data.key is 0
                if not q.empty():
                    while not q.empty():
                        val_with_key = q.get()
                        if encap_data.associated_sign != val_with_key.key:
                            q.put(val_with_key)
                        else:
                            passed_pairs += 1       
                            is_matched_key_found = True
                            break
                    
                    if not is_matched_key_found:
                        failed_pairs += 1
                        val_with_key.fail_reason = "Associate sign not match"
                        encap_data.fail_reason = "Associate sign not match"
                        wrong_data_list.append(val_with_key)
                        wrong_data_list.append(encap_data)
                        break

                    end_time_stamp = encap_data.timestamp
                    start_time_stamp = val_with_key.timestamp
                    end_time = datetime.strptime(end_time_stamp, GlobalTimeFormat)
                    start_time = datetime.strptime(start_time_stamp, GlobalTimeFormat)
                    time_diff = end_time - start_time
                    if  time_diff.total_seconds() * 1000 > EnCapMaxMilliSecsDiff:
                        failed_pairs += 1
                        val_with_key.fail_reason = "Time diff not match"
                        encap_data.fail_reason = "Time diff not match"
                        wrong_data_list.append(val_with_key)
                        wrong_data_list.append(encap_data) 
                else: 
                    failed_pairs += 1
                    encap_data.fail_reason = "No matched key found"
                    wrong_data_list.append(encap_data)
        
        if len(wrong_data_list) > 0:
            print("Single file check error count: ", len(wrong_data_list))
            # Here we make the final record
            sep_record = EncapData("", "", "", "", "", "", "")
            fail_rate = (failed_pairs) /float(passed_pairs + failed_pairs) * 100
            fail_rate_str = "{}%".format(fail_rate)
            final_record = EncapData("Pass Pairs: ", passed_pairs, "Fail Pairs", failed_pairs, "", "Fail Rate: ", fail_rate_str)
            wrong_data_list.append(sep_record)
            wrong_data_list.append(final_record)

            wrong_single_f_df = pd.DataFrame.from_records(wrong_data_list)
            wrong_single_f_df.columns = ["timestamp", "position_msg", "sign_name", "sign_type", \
                                            "sign_value", "associate_sign_type", "fail_reason"]
            
            
            wrong_single_f_df.to_excel(self.excel_writer, index=False, sheet_name="Single_File_Check")
        else:
            print("Single file check successfully!")


    def compare_two_files(self):
        encap_target_df = pd.DataFrame.from_records(self.encap_data_ls_target)
        encap_expect_df = pd.DataFrame.from_records(self.encap_data_ls_expect)
        encap_target_df.columns = ["timestamp_target", "position_msg", "sign_name", "sign_type", \
                                            "sign_value_target", "associate_sign_type", "fail_reason"]
        
        encap_expect_df.columns = ["timestamp_expect", "position_msg", "sign_name", "sign_type", \
                                            "sign_value_expect", "associate_sign_type", "fail_reason"]
        
        encap_target_df = encap_target_df[["timestamp_target", "position_msg", "sign_type", \
                                          "sign_value_target"]]
        
        encap_target_df = encap_target_df[encap_target_df["sign_type"] != 0]
        
        
        encap_expect_df = encap_expect_df[["timestamp_expect", "position_msg", "sign_type", \
                                          "sign_value_expect"]]

        encap_expect_df = encap_expect_df[encap_expect_df["sign_type"] != 0]
        
        res_df = pd.merge(encap_target_df, encap_expect_df, on=["position_msg", "sign_type"], how="left")

        res_df = res_df[res_df["sign_type"].isnull()]

        if len(res_df) > 0:
            print("Two files check error count: ", len(res_df))
            res_df.to_excel(self.excel_writer, index=False, sheet_name="Two_Files_Check")
        else:
            print("Two files check successfully!")
    
    def finish_compare(self):
        self.excel_writer.close()